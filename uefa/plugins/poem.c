#include "plugin.h"

char *lines[16] = {
    "I wandered lonely as a cloud\n",
    "That floats on high o'er vales and hills,\n",
    "When all at once I saw a crowd,\n",
    "A host, of golden daffodils;\n",
    "Beside the lake, beneath the trees,\n",
    "Fluttering and dancing in the breeze.\n",
    "Continuous as the stars that shine\n",
    "And twinkle on the milky way,\n",
    "They stretched in never-ending line\n",
    "Along the margin of a bay:\n",
    "Ten thousand saw I at a glance,\n",
    "Tossing their heads in sprightly dance.\n",
    "The waves beside them danced; but they\n",
    "Out-did the sparkling waves in glee:\n",
    "A poet could not but be gay,\n",
    "In such a jocund company:\n",
};

size_t strlen(const char *str)
{
    size_t len = 0;
    while (*(str++)) {
        ++len;
    }
    return len;
}

int strcmp(const char *lhs, const char *rhs)
{
    while (*lhs == *rhs) {
        if (*lhs == 0) {
            return 0;
        }
        ++lhs;
        ++rhs;
    }
    return 1;
}

void uefa_plugin_encode(struct stream *stream)
{
    char input[1024];
    size_t size;
    while ((size = stream_read(stream, input, sizeof(input))) > 0) {
        for (int i = 0; i < size; ++i) {
            char *lo = lines[input[i] % 16];
            char *hi = lines[input[i] / 16];
            stream_write(stream, lo, strlen(lo));
            stream_write(stream, hi, strlen(hi));
        }
    }
}

int uefa_plugin_decode(struct stream *stream)
{
    char buffer[64];
    size_t len = 0;
    int lo = -1;
    while (stream_read(stream, buffer + len, 1) > 0) {
        ++len;
        if (buffer[len-1] == '\n' && len < sizeof(buffer)) {
            buffer[len] = 0;
            int found = 0;
            for (int i = 0; i < sizeof(lines)/sizeof(char*); ++i) {
                if (strcmp(lines[i], buffer) == 0) {
                    found = 1;
                    if (lo == -1) {
                        lo = i;
                    } else {
                        buffer[0] = lo + 16 * i;
                        stream_write(stream, buffer, 1);
                        lo = -1;
                    }
                    break;
                }
            }
            if (!found) {
                return 1;
            }
            len = 0;
        } else if (len >= sizeof(buffer) - 1) {
            return 1;
        }
    }
    return len > 0 ? 1 : 0;
}
