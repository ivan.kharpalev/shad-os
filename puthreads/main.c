#include "uthread.h"
#include <stdio.h>
#include <assert.h>

void output(void* data)
{
    int i;
    for (i = 0; i < 5000; ++i) {
        printf("i=%d, data=%lx\n", i, (unsigned long) data);
    }
}

void test_output()
{
    uthread_start(output, (void*) 0x333);
    uthread_start(output, (void*) 0x777);
    while (!uthread_try_join()) {
        uthread_yield();
    }
    printf("test_output() passed\n");
}

#define SUMMER_ITERS 10000000

void summer(void* data)
{
    int i;
    volatile long *r = (volatile long*) data;
    for (i = 0; i < SUMMER_ITERS; ++i) {
        *r += 1;
    }
}

void test_summer()
{
    long r = 0;
    int n = 4;
    int i;
    for (i = 0; i < n; i++) {
        uthread_start(summer, (void*) &r);
    }
    while (!uthread_try_join()) {
        uthread_yield();
    }
    assert(r >= SUMMER_ITERS && r < n * SUMMER_ITERS);
    printf("test_summer() passed\n");
}

struct checker_info {
    int id;
    long *holder;
};

void checker(void *data)
{
    volatile struct checker_info *info = (volatile struct checker_info *) data;
    int i;
    *info->holder = info->id;

    for (i = 0; i < 2; i++) {
        int h;
        while ((h = *info->holder) == info->id);
        printf("Expected to find %d in holder, buf found %d\n", info->id, h);
        *info->holder = info->id;
    }
}

void test_overwrite()
{
    long holder = 0;
    struct checker_info checker1 = {1, &holder};
    struct checker_info checker2 = {2, &holder};
    uthread_start(checker, (void*) &checker1);
    uthread_start(checker, (void*) &checker2);
    while (!uthread_try_join()) {
        uthread_yield();
    }
    printf("test_overwrite() passed\n");
}



void g(void* data)
{
    int i;
    for (i = 0; i < 3; ++i) {
        printf("In g(), data=%lx\n", (unsigned long) data);
        uthread_yield();
    }
}

void f(void* data)
{
    int i;
    for (i = 0; i < 3; ++i) {
        printf("In f(), i=%d, data=%lx\n", i, (unsigned long) data);
        uthread_start(g, data + i + 1);
        uthread_yield();
    }
}

void test_nested()
{
    printf("In main()\n");
    uthread_start(f, (void*) 0x555);
    while (!uthread_try_join()) {
        printf("In main()\n");
        uthread_yield();
    }
    printf("test_nested() passed\n");
}

int main()
{
    uthread_init();


    test_output();
    test_overwrite();
    test_summer();
    // Disable this test since it required concurrent queue modifications.
    //test_nested();
    return 0;
}

