#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

struct token
{
    char *start;
    size_t len;
    struct token *next;
};

struct tokenizer
{
    size_t token_count;
    struct token *head;
};

void tokenizer_init(struct tokenizer *tokenizer, char *line)
{
    tokenizer->token_count = 0;
    tokenizer->head = NULL;

    char *next = line;
    struct token *last_token = NULL;

    for (;;) {
        while (*next == ' ' || *next == '\t' || *next == '\n') {
            ++next;
        }

        if (*next == '\0') {
            break;
        }

        char *start = next;
        while (*next != ' ' && *next != '\t' && *next != '\n') {
            ++next;
        }

        struct token *token = (struct token*)malloc(sizeof(struct token));
        token->start = start;
        token->len = next - start;
        token->next = NULL;

        if (last_token) {
            last_token->next = token;
        } else {
            tokenizer->head = token;
        }

        last_token = token;
        ++tokenizer->token_count;
    }
}

void tokenizer_free(struct tokenizer *tokenizer)
{
    struct token *next = tokenizer->head;
    while (next) {
        struct token *prev = next;
        next = next->next;
        free(prev);
    }
}

int get_user_line(char *line, size_t maxlen)
{
    printf("$ ");
    return getline(&line, &maxlen, stdin);
}

int main()
{
    char line[1024];
    setbuf(stdin, NULL); // Disable stdin buffering

    ssize_t len;
    struct tokenizer tokenizer;

    while ((len = get_user_line(line, sizeof(line))) > 0) {

        tokenizer_init(&tokenizer, line);

        // Your code goes here

        tokenizer_free(&tokenizer);
    }

    printf("\n");

    return 0;
}
